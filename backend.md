# Documentation for Online Chat Application

Author(s): Himanshu Yadav, Rahul Yadav & Bharti Sawaria

Last Updated: 05 February 2021, 2:00 PM

## Table of Contents

[[_TOC_]]



# DataBase Management System

The application will have the following Object Models:

## User Model

|Properties         |Data Type                        |Example                                                                              |
|-------------------|---------------------------------|-------------------------------------------------------------------------------------|
|userID             |`ObjectID`                       |`507f1f77bcf86cd799439011`                                                           |
|userName           |`String`                         | Himanshu Yadav                                                                      |
|userProfilePicURL  |`String`                         | http://s3.amazonaws.com/hyperchat/profile-pics/507f1f.jpg                           |
|userFriendIDs      |`Array[UserIDs]`                 |<pre>[<br>  507f1f77bcf86cd799439011,<br>  507f1f77bcf86cd799439022<br>]</pre>       |
|userChannelIDs     |`Array[ChannelIDs]`              |<pre>[<br>  507f1f77bcf86cd799420123,<br>  507f1f77bcf86cd799420124<br>]</pre>       |
|lastSeen           |`String`                         |`Online` or `21/02/2021 09:33PM`                                                     |

## Channel Model

|Properties         |Data Type                      |Example                                                                            |
|-------------------|-------------------------------|-----------------------------------------------------------------------------------|
|channelID          |`ObjectID`                     |`507f1f77bcf86cd799439011`                                                         |
|channelName        |`String`                       |#interest-movies                                                                   |
|channelCreatedBy   |`String`                       |`507f1f77bcf86cd799439011`                                                         |
|channelDesc        |`String`                       |"For all the binge watchers :p"                                                    |
|channelSubscribers |`Array[UserIDs]`               |<pre>[<br>  507f1f77bcf86cd799420123,<br>  507f1f77bcf86cd799420124<br>]</pre>     |

## Message Model

|Properties       |Data Type                                    |Example                                                                            |
|-----------------|---------------------------------------------|-----------------------------------------------------------------------------------|
|messageID        |`ObjectID`                                   |`507f1f77bcf86cd799439011`                                                         |
|messageType      |`String`                                     |`image` or `text`                                                                  |
|isChannelMessage |`Boolean`                                    |`true` or `false`                                                                  |
|senderID         |`UserID`                                     |`507f1f77bcf86cd799439011`                                                         |
|recieverID       |`ObjectID(UserID or ChannelID)`              |`507f1f77bcf86cd799439011`                                                         |
|sentTime         |`Date`                                       |`21/02/2021 09:33PM`                                                               |
|isEdited         |`Boolean`                                    |`true` or `false`                                                                  |
|messagePayload   |`String`                                     |`Required help at Vietnam S2 Instance` or `hyperchat/507f1f/21012021.jpg`          |
|deliveredTime    |`Array[ {userID, deliveredTime} ]`  |<pre>[<br>  {<br>    userID: "507f2",<br>    delTime: "09:33PM"<br>  }, {<br>    userID: "507f1",<br>    delTime: "09:40PM"<br>  }<br>]          |
|seenTime         |`Array[ {userID, seenTime} ]` |<pre>[<br>  {<br>    userID: "507f2",<br>    seenTime: "09:33PM"<br>  }, {<br>    userID: "507f1",<br>    seenTime: "09:40PM"<br>  }<br>]         |


# API Documentation

This section lists all the APIs that will help in completion of the project.

We assume that the developers are using appropriate headers when using these APIs.

**Note:**

For a POST request, the "headers" object in request should include specific headers if there is a body attached to the requests.

|Key |Value |
|----|------|
| Content-type | `application/json` or `multipart/form-data` |

For every API, API-specific errors are stated. If there is no error as stated, and the response is not OK, it means the error is `500 Internal Server Error` in that case, the response will be:

|Key |Value |
|----|------|
| errorMessage | `Internal Server Error, please try again!` |

## User APIs

APIs related to all user based functionalities. These APIs will have `/user` prepended before API name (eg. `/user/registerNewUser`).

- ### registerNewUser

<pre><b>POST</b> /user/registerNewUser</pre>

1. Register a new user with this API.
2. Validations for registering a new user are listed [here](validation.md#user-registration-validations).
3. If request is successful, client gets a response with a JWT token and user basic details.
4. If validation fails, client gets an `errorMessage` in response.

**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| userName | `String` | Himanshu Yadav |
| userEmail | `String` | himanshu@hyperverge.co |
| userPassword |`String` | Test@123 |
| userConfirmPassword |`String` | Test@123 |
| userProfilePic |`File` | `encoded 64-bit image file` |

**Outgoing Response Body** (Default `Status: 201 Created`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | User created successfully |
| token | `JWT-Token` | `2i3bur23ubil32km` |
| user |`User Object` | <pre>{<br>  _id: 2ee2f3cc,<br>  userName: Himanshu Yadav<br>}</pre> |

**Validation Error**

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Error in User Email` |





- ### loginUser

<pre><b>POST</b> /user/loginUser</pre>

1. Login an existing user with this API.
2. Validations for logging in an existing user are listed [here](validation.md#user-login-validations).
3. If request is successful, client gets a response with a JWT token and user basic details.
4. If validation fails, client gets an `errorMessage` in response.

**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| userEmail | `String` | himanshu@hyperverge.co |
| userPassword |`String` | Test@123 |

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | User logged in successfully |
| token | `JWT-Token` | `2i3bur23ubil32km` |
| user |`User Object` | <pre>{<br>  _id: 2ee2f3cc,<br>  userName: Himanshu Yadav<br>}</pre> |

**Validation Error**

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Error in User Email` |





- ### getAllUsers

<pre><b>GET</b> /user/getAllUsers?offset=0</pre>

1. Get the list of all users in the workspace. This API, _in future_ will implement pagination due to which there is an _offset_ number in query.
2. It will fetch total (offset+20) users. So, initially when offset = 0, it will give 01-20 users, then client will increment offset to 20, and send request again. This time API will provide users 21-40 from the list of all users.
3. If request is successful, client gets a response with a list of users(`count(UsersList)=20`).

**Incoming Request URL query**

|Key |Value |Example |
|----|------|--------|
| offset | `Integer` | `0` or `20`|

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | List of users: 21 to 40 |
| users | `Array[ User Object ]` | <pre>[<br>  {<br>    _id: 2r23,<br>    name: "Bharti",<br>   email: "bharti@gmail.com"<br>  },<br>  {<br>    _id: d35y,<br>    name: "Rahul",<br>   email: "rahul@gmail.com"<br>  }<br>]</pre> |







- ### addNewFriend

<pre><b>PATCH</b> /user/addNewFriend</pre>

1. Add provided friend to user's friend list so that next time user opens the app, the list of Direct Messages will have that friend.
2. If request is successful, client gets a response with a success message.
3. If validation fails, client gets an `errorMessage` in response.

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| authentication | `Token auth_token` | `Token 3fg54g5g543fd2` |


**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| friendID | `ObjectID` | `g544590h` |

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | Friend Added Successfully |

**UserNotFound Error** (`Status: 404 Not Found`)

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Could not find user for provided ID` |










- ### getUserFriendsAndChannels

<pre><b>GET</b> /user/getUserFriendsAndChannels</pre>

1. Returns the list of user's friends and channels to which user is subscribed. 
2. It will pull out userID from header token itself.

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| authentication | `Token auth_token` | `Token 3fg54g5g543fd2` |


**Incoming Request Body**

`No request body`

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| userFriends | `Array[ User Objects ]` | <pre>[<br>  {<br>    _id: 2r23,<br>    name: "Bharti"<br>  },<br>  {<br>    _id: d35y,<br>    name: "Himanshu"<br>  }<br>]</pre> |
| userSubscribedChannels | `Array[ Channel Objects ]` | <pre>[<br>  {<br>    _id: 2r23,<br>    name: "interest-sports"<br>  },<br>  {<br>    _id: d35y,<br>    name: "proj-boom-bee"<br>  }<br>]</pre> |

- ### getAllUserNames

<pre><b>GET</b> /user/getAllUserNames</pre>

1. Returns the list of all users and their IDs only. This API will be called only when user is creating a group and needs a list of subscribers to add initially. 
2. It will list {userID, userName} objects.

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| authentication | `Token auth_token` | `Token 3fg54g5g543fd2` |


**Incoming Request Body**

`No request body`

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| users | `Array[ User Objects ]` | <pre>[<br>  {<br>    _id: 2r23,<br>    name: "Bharti"<br>  },<br>  {<br>    _id: d35y,<br>    name: "Himanshu"<br>  }<br>]</pre> |











## Channel APIs

APIs related to all channel based functionalities. These APIs will have /channel prepended before API name (eg. /channel/getAllChannels).

- ### createNewChannel

<pre><b>POST</b> /channel/createNewChannel</pre>

1. Create a new channel for the workspace. This channel will be a part of the list when user opens channel browser to get all channels.
2. Validations for creating a new channel are listed [here](validation.md#create-channel-validations).
3. If request is successful, client gets a response with a success message.
4. If validation fails, client gets an `errorMessage` in response.

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| authentication | `Token auth_token` | `Token 3fg54g5g543fd2` |


**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| channelName | `String` | `interest-anime` |
| channelDesc | `String` | For all the anime lovers |
| subscribedUserIDs | `Array[ UserIDs ]` | <pre>[<br>  507f1f77bcf86cd799439011,<br>  507f1f77bcf86cd799439022<br>]</pre> |


**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | Added Channel Successfully |

**Validation Error**

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Cannot create channel with empty name` |







- ### joinChannelByID

<pre><b>POST</b> /channel/joinChannelByID</pre>

1. Adds the curresnt user to the list of channel subscribers.
2. If request is successful, client gets a response with a success message.
3. If channelID is not found, client gets an `errorMessage` in response.

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| authentication | `Token auth_token` | `Token 3fg54g5g543fd2` |


**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| channelID | `ObjectID` | `5j5jjoj543poj39g043` |


**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | Subscribed to channel Successfully |

**ChannelNotFound Error** (`Status: 404 Not Found`)

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Could not find channel with provided ID` |










- ### getAllChannels

<pre><b>GET</b> /channel/getAllChannels?offset=0</pre>

1. Get the list of all channels in the workspace. This API, _in future_ will implement pagination due to which there is an _offset_ number in query.
2. It will fetch total (offset+20) channels. So, initially when offset = 0, it will give 01-20 channels, then client will increment offset to 20, and send request again. This time API will provide users 21-40 from the list of all channels.
3. If request is successful, client gets a response with a list of channels(`count(ChannelsList)=20`).

**Incoming Request URL query**

|Key |Value |Example |
|----|------|--------|
| offset | `Integer` | `0` or `20`|

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | List of channels: 21 to 40 |
| channels | `Array[ Channel Object ]` | <pre>[<br>  {<br>    _id: 2r23,<br>    channelName: "random"<br>  },<br>  {<br>    _id: d35y,<br>    channelName: "interest-puppys",<br>  }<br>]</pre> |







## Message APIs

APIs related to all message based functionalities. These APIs will have /message prepended before API name (eg. /message/sendMessage).

- ### sendMessage

<pre><b>POST</b> /message/sendMessage</pre>

1. Sends a message to the server. The _isChannelMessage_ property will tell us whether that message is sent from a group or a personal DM.
2. If the reciever is online, we directly send the message to the reciever through **[WebSockets](socket.io)**. However, if the reciever is not online, we have to use some [third party libraries](https://www.twilio.com/docs/conversations) to send reciever(s) that message.
3. Validations for sending message are listed [here](validation.md#send-message-validations).
4. If request is successful, client gets a response with success message.
5. If the message or any other field is invalid, client gets an `errorMessage` in response. 

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| authentication | `Token auth_token` | `Token 3fg54g5g543fd2` |

**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| messageType | `String` | `image` or `text`|
| messagePayload | `String` or `file` | `Hello friends, chai pi lo` |
| isChannelMessage | `Boolean` | `true` or `false` |
| recieverID | `ObjectID` | `n4hiu324ig90` |
| sentTime | `Date` | `23 Jan 2021 09:30PM` |


**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | `Message Sent Successfully` |

**Validation Error**

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Cannot send message with empty data` |


- ### editMessage

<pre><b>PATCH</b> /message/editMessage</pre>

1. Edits a previously sent message. Sent, Delivered and Seen Times **will not be affected** by this request.
2. If request is successful, client gets a response with success message.
3. If the message or any other field is invalid, client gets an `errorMessage` in response. 

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| authentication | `Token auth_token` | `Token 3fg54g5g543fd2` |

**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| messageID | `ObjectID` | `hoi4i5l34knlop9`|
| messagePayload | `String` | `Hello friends, chai pi lo` |
| isChannelMessage | `Boolean` | `true` or `false` |
| recieverID | `ObjectID` | `n4hiu324ig90` |


**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | `Message Edited Successfully` |

**Validation Error**

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Cannot send message with empty data` |






- ### deleteMessage

<pre><b>DELETE</b> /message/deleteMessage</pre>

1. Deletes a previously sent message. **That message will be permanently removed from database and there will be no logs of it**.
2. If request is successful, client gets a response with success message.

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| authentication | `Token auth_token` | `Token 3fg54g5g543fd2` |

**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| messageID | `ObjectID` | `hoi4i5l34knlop9`|


**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | `Message Deleted Successfully` |
