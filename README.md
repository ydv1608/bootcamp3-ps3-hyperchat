# Documentation for Online Chat Application

Author(s): Himanshu Yadav, Rahul Yadav & Bharti Sawaria  

Last Updated: 10 February 2021, 2:12 PM

## Table of Contents

[[_TOC_]]

# Introduction

## Overview

This document is to be treated as the Software Design Document for this chat application project **HyperChat**.

This document describes various system requirements, user interfaces, features and functionalities of HyperChat in detail.

At the end of the document, there is a schedule which is to be followed during creation of the project. The tasks are assigned priority and to be worked upon accordingly.

## Scope

A scalable, user-friendly chat application which has the basic functionalities of messaging, communication through DMs and by creating user channels where a group of users can cluster and interact.

# Requirements/System Overview

## Functional Requirements

|Requirement ID |Statement | Importance(P0/P1/P2) |
|---------------|----------|----------------------------------|
| HC001 | App must have privacy for user | P0 |
| HC002 | User data must be stored as encrypted(eg. passwords) in a database | P0 |
| HC003 | User can see the list of channels | P0 |
| HC004 | User can see the list of friends | P0 |
| HC005 | User can create channels | P0 |
| HC006 | User can join channels | P0 |
| HC007 | User can add friends | P0 |
| HC008 | User can send message and attachment to other user in DM | P0 |
| HC009 | User can send message and attachment to other user through a channel | P0 |
| HC010 | User can delete or edit their own message | P1 |
| HC011 | Sent, Delivered and Seen time of message will be visible | P1 |
| HC012 | Last seen/Online status will be visible | P1 |
| HC013 | Whether a user is typing in a common channel or in a DM will be visible | P1 |

## Technical Overview(Tech. Stack)

The proposed system will be developed using client/server architecture although communication through sockets will be there.  
Other features:  
- Client: [React.js](https://reactjs.org/) (External libraries: [Material-UI](https://material-ui.com/) for UI/UX)
- Server: [Node.js](https://nodejs.org/en/) (External Libraries: [Express.js](https://expressjs.com/), [Mocha](https://mochajs.org/), [Chai](https://www.chaijs.com/) (for testing))
- Database: (Non-SQL)[MongoDB](https://www.mongodb.com/), [Redis](https://redis.io/) (for recent messages cache)

# Use Case Diagrams

This section will describe all the use case approaches used in the application.

## Use Case- Authentication

This is the use case approach for authentication of any user.

![use-case-authentication](img/UseCaseAuth.jpg)

## Use Case- Connections

This is the use case approach for connections(Friends+ Channels) of any user.

![use-case-connection](img/UseCaseConnection.jpg)

## Use Case- Messaging

This is the use case approach for messaging of any user.

![use-case-message](img/UseCaseMessage.jpeg)


# Activity Diagrams

This section will describe all the activity diagrams used in the application.

## Activity Diagram- Authentication

This is the activity diagram for authentication of any user.

![activity-diagram-authentication](img/ActivityDiagramAuth.jpg)

## Activity Diagram- Connections

This is the activity diagram for connections(Friends+ Channels) of any user.

![activity-diagram-connection](img/ActivityDiagramConnection.jpg)

## Activity Diagram- Messaging

This is the use case approach for messaging of any user.

![activity-diagram-message](img/ActivityDiagramChat.jpg)

# User Interface

The application will have following user-friendly interfaces:  
## Login Page
User will have to login first before using the application
 
## Dashboard Page
It will appear once the user is logged in. It will contain the list of 
- Subscribed Channels
- Friend’s List
- Add friend option
- Add channel option
- Chat Panel
## Search Page
User can search for different channels that he/she wants to join and user can search for other users for the workspace

# Frontend

## Reducers
This section will list all the actions and their types for the client side.
 
### actionType: `FETCH_FRIENDS_AND_CHANNELS`

`Dispatched Action` : fetchFriendsAndChannels  
`Description` : After the user logged in, this action will be dispatched and a request will be sent to the server to fetch the friend list and subscribed channel list of that user.

### actionType: `FETCH_MESSAGES_IN_CHANNEL`

`Dispatched Action` : fetchMessagesInChannel  
`Description` : When the user clicks on a subscribed channel name, this action will be dispatched and a request will be sent to the server to fetch the messages of that channel. The server will send 20 recent messages.

### actionType: `FETCH_MESSAGES_IN_DM`

`Dispatched Action` : fetchMessagesInDM  
`Description` : When the user clicks on his/her friend name, this action will be dispatched and a request will be sent to the server to fetch the messages of that personal chat. The server will send 20 recent messages.

### actionType: `CREATE_NEW_CHANNEL`

`Dispatched Action` : createNewChannel  
`Description` : When the user clicks on the **ADD CHANNEL** button and enters the required details in the add channel form, and then clicks on the final **ADD** button, this action will be dispatched and a request will be sent to the server to add that channel.

### actionType: `JOIN_CHANNEL`

`Dispatched Action` : joinChannel  
`Description` : When a user selects a channel and clicks on the **JOIN** button, this action will be dispatched and a request will be sent to the server to add that channel to the user’s subscribed channel list.

### actionType: `SEND_MESSAGE`

`Dispatched Action` : sendMessage  
`Description` : When the user types a message and clicks on the **SEND** button, this action will be dispatched and a request will be sent to the server. The server will add a message object in DB and socket.io will send that message to the concerned recipient.

### actionType: `EDIT_MESSAGE`

`Dispatched Action` : editMessage  
`Description` : When the user clicks on the message options button, the **EDIT** option will appear. When the user edits the message and confirms the edit, this action will be dispatched and a PATCH request will be sent to the socket, server and send necessary details to DB and receiver side.

### actionType: `DELETE_MESSAGE`

`Dispatched Action` : deleteMessage  
`Description` : When the user clicks on the message options, the **DELETE** option will appear. When the user deletes the message and confirms the delete, this action will be dispatched and a delete request will be sent to the server. The server will delete that message from the database and inform it to the recipient.

# DataBase Management System

The application will have the following Object Models:

## User Model

|Properties         |Data Type                        |Example                                                                              |
|-------------------|---------------------------------|-------------------------------------------------------------------------------------|
|_id                |`ObjectID`                       |`507f1f77bcf86cd799439011`                                                           |
|userName           |`String`(required)               | Himanshu Yadav                                                                      |
|userEmail          |`String`(required)               | himanshu@gmail.com                                                                  |
|userPassword       |`String`(required)               | Testing@123Test                                                                     |
|userProfilePicURL  |`String`                         | http://s3.amazonaws.com/hyperchat/profile-pics/507f1f.jpg                           |
|userFriendIDs      |`Array[UserIDs]`                 |<pre>[<br>  507f1f77bcf86cd799439011,<br>  507f1f77bcf86cd799439022<br>]</pre>       |
|userChannelIDs     |`Array[ChannelIDs]`              |<pre>[<br>  507f1f77bcf86cd799420123,<br>  507f1f77bcf86cd799420124<br>]</pre>       |
|lastSeen           |`String`                         |`Online` or `21/02/2021 09:33PM`                                                     |

## Channel Model

|Properties         |Data Type                      |Example                                                                            |
|-------------------|-------------------------------|-----------------------------------------------------------------------------------|
|_id                |`ObjectID`                     |`507f1f77bcf86cd799439011`                                                         |
|channelName        |`String`(required)             |#interest-movies                                                                   |
|channelCreatedBy   |`String`(required)             |`507f1f77bcf86cd799439011`                                                         |
|channelDesc        |`String`(required)             |"For all the binge watchers :p"                                                    |
|channelSubscribers |`Array[UserIDs]`               |<pre>[<br>  507f1f77bcf86cd799420123,<br>  507f1f77bcf86cd799420124<br>]</pre>     |

## Message Model

|Properties       |Data Type                                    |Example                                                                            |
|-----------------|---------------------------------------------|-----------------------------------------------------------------------------------|
|_id              |`ObjectID`                                   |`507f1f77bcf86cd799439011`                                                         |
|messageType      |`String`(required)                           |`image` or `text`                                                                  |
|isChannelMessage |`Boolean`(required)                          |`true` or `false`                                                                  |
|senderID         |`UserID`(required)                           |`507f1f77bcf86cd799439011`                                                         |
|receiverID       |`ObjectID(UserID or ChannelID)`(required)    |`507f1f77bcf86cd799439011`                                                         |
|sentTime         |`Date`(required)                             |`21/02/2021 09:33PM`                                                               |
|isEdited         |`Boolean`                                    |`true` or `false`                                                                  |
|isDeleted        |`Boolean`                                    |`true` or `false`                                                                  |
|messagePayload   |`String`(required)                           |`Required help at Vietnam S2 Instance` or `hyperchat/507f1f/21012021.jpg`          |
|deliveredTime    |`Array[ {userID, deliveredTime} ]`  |<pre>[<br>  {<br>    userID: "507f2",<br>    delTime: "09:33PM"<br>  }, {<br>    userID: "507f1",<br>    delTime: "09:40PM"<br>  }<br>]          |
|seenTime         |`Array[ {userID, seenTime} ]` |<pre>[<br>  {<br>    userID: "507f2",<br>    seenTime: "09:33PM"<br>  }, {<br>    userID: "507f1",<br>    seenTime: "09:40PM"<br>  }<br>]         |


# API Documentation

This section lists all the APIs that will help in completion of the project.

We assume that the developers are using appropriate headers when using these APIs.

**Note:**

For a POST request, the "headers" object in request should include specific headers if there is a body attached to the requests.

|Key |Value |
|----|------|
| Content-type | `application/json` or `multipart/form-data` |

For every API, API-specific errors are stated. If there is no error as stated, and the response is not OK, it means the error is `500 Internal Server Error` in that case, the response will be:

|Key |Value |
|----|------|
| errorMessage | `Internal Server Error, please try again!` |

## User APIs

APIs related to all user based functionalities. Includes finding other users, sending DMs to other users.

- ### registerNewUser

<pre><b>POST</b> /auth/user/register</pre>

1. Register a new user with this API.
2. Validations for registering a new user are listed [here](validation.md#user-registration-validations).
3. If request is successful, client gets a response with a JWT token and user's basic details.
4. If validation fails, client gets an `errorMessage` in response.

**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| userName | `String` | Himanshu Yadav |
| userEmail | `String` | himanshu@hyperverge.co |
| userPassword |`String` | Test@123 |
| userConfirmPassword |`String` | Test@123 |
| userProfilePic |`File` | `encoded 64-bit image file` |

**Outgoing Response Body** (Default `Status: 201 Created`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | User created successfully |
| token | `JWT-Token` | `2i3bur23ubil32km` |
| user |`User Object` | <pre>{<br>  _id: 2ee2f3cc,<br>  userName: Himanshu Yadav<br>}</pre> |

**Validation Error**

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Error in User Email` |





- ### loginUser

<pre><b>POST</b> /auth/user/login</pre>

1. Log in an existing user with this API.
2. Validations for logging in an existing user are listed [here](validation.md#user-login-validations).
3. If request is successful, client gets a response with a JWT token and user's basic details.
4. If validation fails, client gets an `errorMessage` in response.

**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| userEmail | `String` | himanshu@hyperverge.co |
| userPassword |`String` | Test@123 |

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | User logged in successfully |
| token | `JWT-Token` | `2i3bur23ubil32km` |
| user |`User Object` | <pre>{<br>  _id: 2ee2f3cc,<br>  userName: Himanshu Yadav<br>}</pre> |

**Validation Error**

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Error in User Email` |





- ### getAllUsers

<pre><b>GET</b> /users?limit=20&offset=0&fields=userName,id,userProfilePicURL</pre>

1. Get the list of some users in the workspace.
2. It will fetch a list of users. One at a time, the length of users sent is equal to limit value provided by client(default=20). So, initially when offset = 0, it will give 01-20 users, then client will increment offset to 20, and send request again. This time API will provide users 21-40 from the list of all users.
3. If request is successful, client gets a response with a list of users(`count(UsersList)=20=limit`).

**Incoming Request URL query**

|Key |Value |Example |
|----|------|--------|
| limit | `Integer` | `20`|
| offset | `Integer` | `0`|

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | List of users: 21 to 40 |
| users | `Array[ User Object ]` | <pre>[<br>  {<br>    _id: 2r23,<br>    name: "Bharti",<br>    pic: "https://s2.aws.com/pic1.jpg"<br>  },<br>  {<br>    _id: d35y,<br>    name: "Rahul",<br>    pic: "https://s2.aws.com/pic1.jpg"<br>  }<br>]</pre> |







- ### addNewDM

<pre><b>PUT</b> /users/uid/dm/add</pre>

1. Add another user to current user's DM list so that next time user opens the app, the list of Direct Messages will have that particular user with which he interacted.
2. If request is successful, client gets a response with a success message.
3. If validation fails, client gets an `errorMessage` in response.

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| Authorization | `Token auth_token` | `Token 3fg54g5g543fd2` |


**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| DmID | `ObjectID` | `g544590h` |

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | Added in User's recent DM Successfully |

**UserNotFound Error** (`Status: 404 Not Found`)

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Could not find user for provided ID` |










- ### getUserDMsAndChannels

<pre><b>GET</b> /users/uid?fields=channels,DMs</pre>

1. Returns the list of user's recent DMs and channels to which user is subscribed. 
2. It will pull out userID from header token itself.

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| Authorization | `Token auth_token` | `Token 3fg54g5g543fd2` |


**Incoming Request Body**

`No request body`

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| userFriends | `Array[ User Objects ]` | <pre>[<br>  {<br>    _id: 2r23,<br>    name: "Bharti"<br>  },<br>  {<br>    _id: d35y,<br>    name: "Himanshu"<br>  }<br>]</pre> |
| userSubscribedChannels | `Array[ Channel Objects ]` | <pre>[<br>  {<br>    _id: 2r23,<br>    name: "interest-sports"<br>  },<br>  {<br>    _id: d35y,<br>    name: "proj-boom-bee"<br>  }<br>]</pre> |

- ### getUserDmMessages

<pre><b>GET</b> /users/uid/dm/dmid/message?limit=20&offset=0</pre>

1. Get the list of recent messages with another user.
2. It will fetch a list of recent chats with another user. One at a time, total messages sent in response is equal to limit value provided by client(default=20). So, initially when offset = 0, it will give most recent 01-20 messages, then client will increment offset to 20, and send request again. This time API will provide recent 21-40 messages from the database.
3. If request is successful, client gets a response with a list of messages(`count(MessagesList)=20=limit`).

**Incoming Request URL query**

|Key |Value |Example |
|----|------|--------|
| limit | `Integer` | `0` or `20`|
| offset | `Integer` | `0` or `20`|

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | List of messages: 21 to 40 |
| message-list | `Array[ Message Object ]` | <pre>[<br>  {<br>    _id: 2r23,<br>    message: "random m1"<br>  },<br>  {<br>    _id: d35y,<br>    message: "message m2",<br>  }<br>]</pre> |



- ### sendMessageInDM

<pre><b>POST</b> /users/uid/dm/messsage</pre>

1. Sends a message to another user.
2. If the receiver is online, we directly send the message to the receiver through **[WebSockets](socket.io)**. However, if the receiver is not online, we have to use some [third party libraries](https://www.twilio.com/docs/conversations) to send receiver(s) that message through notifications. In both the cases, messages are stored first in database.
3. Validations for sending message are listed [here](validation.md#send-message-validations).
4. If request is successful, client gets a response with success message.
5. If the message or any other field is invalid, client gets an `errorMessage` in response. 

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| Authorization | `Token auth_token` | `Token 3fg54g5g543fd2` |

**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| messageType | `String` | `image` or `text`|
| messagePayload | `String` or `file` | `Hello friends, chai pi lo` |
| receiverID | `ObjectID` | `n4hiu324ig90` |
| sentTime | `Date` | `23 Jan 2021 09:30PM` |


**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| response | `String` | `Message Sent Successfully` |
| message | `Message Object` | <pre>{<br>  _id: 2r23,<br>  messagePayload: "random m1",<br>  isEdited: false,<br>  isDeleted: false<br>}</pre> |

**Validation Error**

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Cannot send message with empty data` |



- ### editMessageInDM

<pre><b>PUT</b> /users/uid/dm/messsage</pre>

1. Edits a previously sent message. Sent, Delivered and Seen Times **will not be affected** by this request.
2. If request is successful, client gets a response with success message.
3. If the message or any other field is invalid, client gets an `errorMessage` in response. 

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| Authorization | `Token auth_token` | `Token 3fg54g5g543fd2` |

**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| messageID | `ObjectID` | `hoi4i5l34knlop9`|
| messagePayload | `String` | `Hello friends, chai pi lo` |
| receiverID | `ObjectID` | `n4hiu324ig90` |


**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| response | `String` | `Message Edited Successfully` |
| message | `Message Object` | <pre>{<br>  _id: 2r23,<br>  messagePayload: "random m1",<br>  isEdited: true,<br>  isDeleted: false<br>}</pre> |

**Validation Error**

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Cannot send message with empty data` |






- ### deleteMessageInDM

<pre><b>DELETE</b> /users/uid/dm/message/messageID</pre>

1. Deletes a previously sent message. That message will **not** be permanently removed from database and there will be no logs of it.
2. Only the isDeleted property of message will become true. This is implemented if in case, further we want to add _threads_ functionality to the app.
2. If request is successful, client gets a response with success message.

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| Authorization | `Token auth_token` | `Token 3fg54g5g543fd2` |

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| response | `String` | `Message Deleted Successfully` |
| success | `Boolean`(always true otherwise error will come) | `true` |








## Channel APIs

APIs related to all channel based functionalities. These APIs will have /channel prepended before API name (eg. /channels).

- ### createChannel

<pre><b>POST</b> /channels</pre>

1. Create a new channel for the workspace. This channel will be a part of the list when user opens channel browser to get all channels.
2. Validations for creating a new channel are listed [here](validation.md#create-channel-validations).
3. If request is successful, client gets a response with a success message.
4. If validation fails, client gets an `errorMessage` in response.

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| Authorization | `Token auth_token` | `Token 3fg54g5g543fd2` |


**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| channelName | `String` | `interest-anime` |
| channelDesc | `String` | For all the anime lovers |
| subscribedUserIDs | `Array[ UserIDs ]` | <pre>[<br>  507f1f77bcf86cd799439011,<br>  507f1f77bcf86cd799439022<br>]</pre> |


**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | Added Channel Successfully |

**Validation Error**

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Cannot create channel with empty name` |







- ### joinChannelByID

<pre><b>POST</b> /channels/cid/join</pre>

1. Add the current user to the channel.
2. If request is successful, client gets a response with a success message.
3. If channelID is not found, client gets an `errorMessage` in response.

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| Authorization | `Token auth_token` | `Token 3fg54g5g543fd2` |


**Incoming Request Body**

No incoming body.


**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | Subscribed to channel Successfully |

**ChannelNotFound Error** (`Status: 404 Not Found`)

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Could not find channel with provided ID` |




- ### getChannelMessages

<pre><b>GET</b> /channels/cid/message?limit=20&offset=0</pre>

1. Get the list of all messages in a channel.
2. It will fetch total (offset+limit) channel messages. So, initially when offset = 0, it will give 01-20 channel messages, then client will increment offset to 20, and send request again.
3. If request is successful, client gets a response with a list of channels(`count(ChannelMessageList)=20`).

**Incoming Request URL query**

|Key |Value |Example |
|----|------|--------|
| limit  | `Integer` | `20`|
| offset | `Integer` | `0` |

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | List of messages: 01 to 20 |
| messages | `Array[ message Object ]` | <pre>[<br>  {<br>    _id: 2r23,<br>    message: "Hi"<br>  },<br>  {<br>    _id: d35y,<br>    message: "hello",<br>  }<br>]</pre> |







- ### getAllChannels

<pre><b>GET</b> /channels?limit=20&offset=0</pre>

1. Get the list of all channels in the workspace.
2. It will fetch total (offset + limit) channels. So, initially when offset = 0, it will give 01-20 channels, then client will increment offset to 20, and send request again. This time API will provide users 21-40 from the list of all channels.
3. If request is successful, client gets a response with a list of channels(`count(ChannelsList)=20`).

**Incoming Request URL query**

|Key |Value |Example |
|----|------|--------|
| limit  | `Integer`  | `20`|
| offset | `Integer`  | `0` |

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| message | `String` | List of channels: 01 to 20 |
| channels | `Array[ Channel Object ]` | <pre>[<br>  {<br>    _id: 2r23,<br>    channelName: "random"<br>  },<br>  {<br>    _id: d35y,<br>    channelName: "interest-puppys",<br>  }<br>]</pre> |




- ### sendMessageInChannel

<pre><b>POST</b> /channels/cid/message</pre>

1. Sends a message to the server. The _isChannelMessage_ property will tell us whether that message is sent from a group or a personal DM.
2. If the receiver is online, we directly send the message to the receiver through **[WebSockets](socket.io)**. However, if the receiver is not online, we have to use some [third party libraries](https://www.twilio.com/docs/conversations) to send receiver(s) that message.
3. Validations for sending message are listed [here](validation.md#send-message-validations).
4. If request is successful, client gets a response with success message.
5. If the message or any other field is invalid, client gets an `errorMessage` in response. 

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| Authorization | `Token auth_token` | `Token 3fg54g5g543fd2` |

**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| messageType | `String` | `image` or `text`|
| messagePayload | `String` or `file` | `Hello friends, chai pi lo` |
| sentTime | `Date` | `23 Jan 2021 09:30PM` |


**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| response | `String` | `Message Sent Successfully` |
| message | `Message Object` | <pre>{<br>  _id: 2r23,<br>  messagePayload: "random m1",<br>  isEdited: false,<br>  isDeleted: false<br>}</pre> |

**Validation Error**

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Cannot send message with empty data` |



- ### editMessageInChannel

<pre><b>PUT</b> /channels/cid/message</pre>

1. Edits a previously sent message. Sent, Delivered and Seen Times **will not be affected** by this request.
2. If request is successful, client gets a response with success message.
3. If the message or any other field is invalid, client gets an `errorMessage` in response. 

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| Authorization | `Token auth_token` | `Token 3fg54g5g543fd2` |

**Incoming Request Body**

|Key |Value |Example |
|----|------|--------|
| messageID | `ObjectID` | `hoi4i5l34knlop9`|
| messagePayload | `String` | `Hello friends, chai pi lo` |


**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| response | `String` | `Message Edited Successfully` |
| message | `Message Object` | <pre>{<br>  _id: 2r23,<br>  messagePayload: "random m1",<br>  isEdited: true,<br>  isDeleted: false<br>}</pre> |

**Validation Error**

|Key |Value |Example |
|----|------|--------|
| errorMessage | `String` | `Cannot send message with empty data` |






- ### deleteMessageInChannels

<pre><b>DELETE</b> /channels/cid/message/messageID</pre>

1. Deletes a previously sent message. **That message will be permanently removed from database and there will be no logs of it**.
2. If request is successful, client gets a response with success message.

**Incoming Request Headers**

|Key |Value |Example |
|----|------|--------|
| Authorization | `Token auth_token` | `Token 3fg54g5g543fd2` |

**Outgoing Response Body** (Default `Status: 200 OK`)

|Key |Value |Example |
|----|------|--------|
| response | `String` | `Message Deleted Successfully` |
| success | `Boolean`(always true otherwise error will come) | `true` |







# Socket APIs

This section will discuss about socket.io implementations we will be having in our application.

## Initial Startup

After user logs in, he will be sent to dashboard where all the channel and DM list will appear throught [this](#getuserfriendsandchannels) and [this](getUnreadMessageAPI).

After all re-renders(updating unread message in channels+DM), the user will be joining a personal room with ID as userID and will join all rooms with ID as channelIDs and the lastSeen status will be updated.

```javascript
//server-side

    //join all channel-dedicated rooms and create a personal room to receive DMs
    socket.on('USER_JOINED', (channelIDs, userID) => {
        socket.join(userId);
        channelIDs.map((channelID) => {
            socket.join(channelID);
        });
        currentUser = User.findById(userID);
        currentUser.lastSeen = 'Online';
    });

//client-side

    //when ever user connects to the socket
    socket.emit('USER_JOINED', (channelIDS, userID));
 ```

 When user logs out, all joined rooms and personal room will be left.

 ```javascript
//server-side

    //custom event
    socket.on('disconnecting', (userID) => {
        currentUser = User.findById(userID);
        currentUser.lastSeen = new Date().toString();
 
        //leave all rooms 
        socket.rooms.map((roomID) => {
            socket.leave(roomID);
        });
        socket.leave(userID);
    });
```

## Send Message

Sending message is done through 2 different events: DIRECT_MESSAGE(for sending DMs) and CHANNEL_MESSAGE(for sending messages to channels).
After sending the message, the sender will get the message in response(if successful) so that sender's UI can be updated. The receiver side will get socket event which will be handled.

```javascript
//server-side
    
    //direct messages
    io.to(receiverID).emit('DIRECT_MESSAGE', message);
 
    //channel message
    io.to(channelID).broadcast('CHANNEL_MESSAGE', message);

//client-side

    //direct message
    socket.on('DIRECT_MESSAGE', (message));

    //channel messsage
    socket.on('CHANNEL_MESSAGE', (message));
```

**Send Message in DM: Flow**

![sendMessageDM](img/sendMessageDM.png)

**Send Message in Channel: Flow**

![sendMessageChannel](img/sendMessageChannel.png)

## Message Seen

Message seen status is done through 2 different events. When a user opens a channel, every user subscribed to the channel will be sent a socket message so that it's chat gets updated. When opening a DM, the chat's seen statuses will get updated on both users' devices.

```javascript
//server-side

    //direct message seen
    socket.on('DIRECT_MESSAGE_SEEN', (receiverID, seenID) => {
        socket.to(receiverID).emit('DIRECT_MESSAGE_SEEN', { seenID: seenID });
    });
 
    //channel message seen
    socket.on('CHANNEL_MESSAGE_SEEN', (channelID, seenID) => {
        socket.to(channelID).emit('CHANNEL_MESSAGE_SEEN', { seenID: seenID });
    });

//client-side
    
    //direct message seen
    socket.emit('DIRECT_MESSAGE_SEEN', (receiverID, seenID));
    socket.on('DIRECT_MESSAGE_SEEN', (seenID));
    
    //channel message seen
    socket.emit('CHANNEL_MESSAGE_SEEN', (channelID, seenID));
    socket.on('CHANNEL_MESSAGE_SEEN', (seenID));
```

## Message Edit

When a message is edited, it will send a request to socket so it will get updated at receiver's end(in DMs) or other subscriber's end(in channels).
The sender, who is also the editor, will get message object in response of request, hence he can update from this (response.message).

```javascript
//server-side

    //edit message in dms
    io.to(receiverID).emit('EDIT_MESSAGE_DM', message);
    
    //edit message in channels
    io.to(channelID).emit('EDIT_MESSAGE_CHANNEL', message);

//client-side
    
    //edit message in dms
    socket.on('EDIT_MESSAGE_DM', (message));

    //edit message in channels
    socket.on('EDIT_MESSAGE_CHANNEL', (message));
```

## Message Delete

When a message is deleted, a socket notification will be sent and the message will get updated at receiver's end(in DMs) or other subscriber's end(in channels).
The sender, who is also the remover, will get success object in response of request, hence he can update from this (response.success).
The deletion process is a **soft delete** which means the message won't be deleted from DB but only it's details will be removed so that if there is a thread, it will be preserved.

```javascript
//server-side

    //delete message in dms
    io.to(receiverID).emit('DELETE_MESSAGE_DM', message);
    
    //delete message in channels
    io.to(channelID).emit('DELETE_MESSAGE_CHANNEL', message);

//client-side
    
    //delete message in dms
    socket.on('DELETE_MESSAGE_DM', (message));

    //delete message in channels
    socket.on('DELETE_MESSAGE_CHANNEL', (message));
```

## User Typing Notification

When a user is typing a message, a socket message will be shared to other user(in DMs) or users(in channels) by showing a triple dot image (in slack) or by writing "xyz is typing..." below channel/DM name.

```javascript
//server-side

    //typing in dms
    socket.on('TYPING_DM', (senderID, receiverID) => {
        socket.to(receiverID).broadcast('TYPING_DM', {
            senderID: senderID,
        });
    });
 
    //typing in channels
    socket.on('TYPING_CHANNEL', (senderID, channelID) => {
        socket.to(channelID).broadcast('TYPING_CHANNEL', {
            senderID: senderID,
        });
    });


//client-side
    
    //typing message in dms
    socket.emit('TYPING_DM', ((senderID, receiverID));
    socket.on('TYPING_DM', (senderID));
 
    //typing message in channels
    socket.emit('TYPING_CHANNEL', (senderID, channelID));
    socket.on('TYPING_GROUP', (senderID));
```

# Milestones

> If you’re not embarrassed by the first version of your product, you’ve launched too late.

The following high-level milestones are prepared right now. These might change or split into sub-milestones during live development.

1. Facade application showing screen with temporary transitions and example images / text.
2. Communication protocol: application connects to network / server.
3. Stand-alone working backend: to be tested by Postman.
4. Adding **Must** Requirements from [Functional Requirements](#functional-requirements)
5. Upgrading UI/UX on client side.
6. Adding **Want** requirements from [Functional Requirements](#functional-requirements)
7. Alpha application (with full functionality)
8. Internal Testing (Separate for frontend, backend, Unit + Integration Testing)
9. Stability and Optimization
10. Release Beta

# Prioritization

|Requirement ID |Statement | Priority | Status |
|---------------|----------|----------|--------|
| HC000 | Review of the Documentation | 0 | :heavy_check_mark: |
| HC001 | App must have privacy for user | 1 |  :heavy_check_mark: |
| HC002 | User data must be stored as encrypted(eg. password) in a database | 2 | :x: |
| HC003 | User can see the list of channels | 2 | :x: |
| HC004 | User can see the list of friends | 2 | :x: |
| HC005 | User can create channel | 2 | :x: |
| HC006 | User can join channel | 3 | :x: |
| HC007 | User can add friend | 2 | :x: |
| HC008 | User can send message and attachment to other user in DM | 2 | :x: |
| HC009 | User can send message and attachment to other user through a channel | 2 | :x: |
| HC010 | User can delete or edit their own message | 4 | :x: |
| HC011 | Sent, Delivered and Seen time of message will be visible | 4 | :x: |
| HC012 | Last seen/Online status will be visible | 5 | :x: |
| HC013 | Whether a user is typing in a common channel or in a DM will be indicated by a symbol | 5 | :x: |
