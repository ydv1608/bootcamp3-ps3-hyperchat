## Table of Contents

[[_TOC_]]

# Socket APIs

This section will discuss about socket.io implementations we will be having in our application.

## Initial Startup

After user logs in, he will be sent to dashboard where all the channel and DM list will appear throught [this](#getuserfriendsandchannels) and [this](getUnreadMessageAPI).

After all re-renders(updating unread message in channels+DM), the user will be joining a personal room with ID as userID and will join all rooms with ID as channelIDs and the lastSeen status will be updated.

```javascript
//server-side

    //join all channel-dedicated rooms and create a personal room to recieve DMs
    socket.on('USER_JOINED', (channelIDs, userID) => {
        socket.join(userId);
        channelIDs.map((channelID) => {
            socket.join(channelID);
        });
        currentUser = User.findById(userID);
        currentUser.lastSeen = 'Online';
    });

//client-side

    //when ever user connects to the socket
    socket.emit('USER_JOINED', (channelIDS, userID));
 ```

 When user logs out, all joined rooms and personal room will be left.

 ```javascript
//server-side

    //custom event
    socket.on('disconnecting', (userID) => {
        currentUser = User.findById(userID);
        currentUser.lastSeen = new Date().toString();
 
        //leave all rooms 
        socket.rooms.map((roomID) => {
            socket.leave(roomID);
        });
        socket.leave(userID);
    });
```

## Send Message

Sending message is done through 2 different events: DIRECT_MESSAGE(for sending DMs) and CHANNEL_MESSAGE(for sending messages to channels).

```javascript
//server-side
    
    //direct messages
    socket.on('DIRECT_MESSAGE', (receiverID, message) => {
        socket.to(receiverID).emit('DIRECT_MESSAGE', { message: message });
    });
 
    //channel message
    socket.on('CHANNEL_MESSAGE', (channelID, message) => {
        socket.to(channelID).broadcast('CHANNEL_MESSAGE', { message: message });
    });

//client-side

    //direct message
    socket.emit('DIRECT_MESSAGE', (receiverID, message));
    socket.on('DIRECT_MESSAGE', (message));

    //channel messsage
    socket.emit('CHANNEL_MESSAGE', (channelID, message));
    socket.on('CHANNEL_MESSAGE', (message));
```

**Send Message in DM: Flow**

![sendMessageDM](img/sendMessageDM.png)

**Send Message in Channel: Flow**

![sendMessageChannel](img/sendMessageChannel.png)

## Message Seen

Message seen status is done through 2 different events. When a user opens a channel, every user subscribed to the channel will be sent a socket message so that it's chat gets updated. When opening a DM, the chat's seen statuses will get updated on both users' devices.

```javascript
//server-side

    //direct message seen
    socket.on('DIRECT_MESSAGE_SEEN', (receiverID, seenID) => {
        socket.to(receiverID).emit('DIRECT_MESSAGE_SEEN', { seenID: seenID });
    });
 
    //channel message seen
    socket.on('CHANNEL_MESSAGE_SEEN', (channelID, seenID) => {
        socket.to(channelID).emit('CHANNEL_MESSAGE_SEEN', { seenID: seenID });
    });

//client-side
    
    //direct message seen
    socket.emit('DIRECT_MESSAGE_SEEN', (receiverID, seenID));
    socket.on('DIRECT_MESSAGE_SEEN', (seenID));
    
    //channel message seen
    socket.emit('CHANNEL_MESSAGE_SEEN', (channelID, seenID));
    socket.on('CHANNEL_MESSAGE_SEEN', (seenID));
```

## Message Edit

When a message is edited, it will send a request to socket so it will update both users'(in DMs) or all users'(in channels) chat will get updated.

```javascript
//server-side

    //edit message in dms
    socket.on('EDIT_MESSAGE_DM', (receiverID, message) => {
        socket.to(receiverID).emit('EDIT_MESSAGE_DM', { message: message });
    });
 
    //edit message in channels
    socket.on('EDIT_MESSAGE_CHANNEL', (channelID, message) => {
        socket.to(channelID).emit('EDIT_MESSAGE_CHANNEL', { message: message });
    });

//client-side
    
    //edit message in dms
    socket.emit('EDIT_MESSAGE_DM', (receiverID, message));
    socket.on('EDIT_MESSAGE_DM', (message));
 
    //edit message in channels
    socket.emit('EDIT_MESSAGE_CHANNEL', (channelID, message));
    socket.on('EDIT_MESSAGE_CHANNEL', (message));
```

## User Typing Notification

When a user is typing a message, a socket message will be shared to other user(in DMs) or users(in channels) by showing a triple dot image (in slack) or by writing "xyz is typing..." below channel/DM name.

```javascript
//server-side

    //typing in dms
    socket.on('TYPING_DM', (senderID, receiverID) => {
        socket.to(receiverID).broadcast('TYPING_DM', {
            senderID: senderID,
        });
    });
 
    //typing in channels
    socket.on('TYPING_CHANNEL', (senderID, channelID) => {
        socket.to(channelID).broadcast('TYPING_CHANNEL', {
            senderID: senderID,
        });
    });


//client-side
    
    //typing message in dms
    socket.emit('TYPING_DM', ((senderID, receiverID));
    socket.on('TYPING_DM', (senderID));
 
    //typing message in channels
    socket.emit('TYPING_CHANNEL', (senderID, channelID));
    socket.on('TYPING_GROUP', (senderID));
```
