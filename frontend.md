# Documentation for Online Chat Application

Author(s): Himanshu Yadav, Rahul Yadav & Bharti Sawaria

Last Updated: 05 February 2021, 2:00 PM

## Table of Contents

[[_TOC_]]


# Introduction

## Overview

This document is to be treated as the Software Design Document for this chat application project **HyperChat**.

This document describes various system requirements, user interfaces, features and functionalities of HyperChat in detail.

At the end of the document, there is a schedule which is to be followed during creation of the project. The tasks are assigned priority and to be worked upon accordingly.

## Scope

A scalable, user-friendly chat application which has the basic functionalities of messaging, peer to peer communication through DMs and by creating user channels where a group of users can cluster and interact.

# Requirements/System Overview

## Functional Requirements

|Requirement ID |Statement | Importance(Must/Wait/Additional) |
|---------------|----------|----------------------------------|
| HC001 | App must have privacy for user | Must |
| HC002 | User data must be stored as encrypted(eg. passwords) in a database | Must |
| HC003 | User can see the list of channels | Must |
| HC004 | User can see the list of friends | Must |
| HC005 | User can create channels | Must |
| HC006 | User can join channels | Must |
| HC007 | User can add friends | Must |
| HC008 | User can send message and attachment to other user in DM | Must |
| HC009 | User can send message and attachment to other user through a channel | Must |
| HC010 | User can delete or edit their own message | Wait |
| HC011 | Sent, Delivered and Seen time of message will be visible | Wait |
| HC012 | Last seen/Online status will be visible | Wait |
| HC013 | Whether a user is typing in a common channel or in a DM will be visible | Wait |

## Technical Overview(Tech. Stack)

The proposed system will be developed using client/server architecture although peer to peer communication will be there too.  
Other features:  
- Client: [React.js](https://reactjs.org/) (External libraries: [Material-UI](https://material-ui.com/) for UI/UX)
- Server: [Node.js](https://nodejs.org/en/) (External Libraries: [Express.js](https://expressjs.com/), [Mocha](https://mochajs.org/), [Chai](https://www.chaijs.com/) (for testing))
- Database: (Non-SQL)[MongoDB](https://www.mongodb.com/), [Redis](https://redis.io/) (for recent messages cache)
- Peer to peer communication(client <-> server) will be done through [socket.io](https://socket.io/)

# Use Case Diagrams

This section will describe all the use case approaches used in the application.

## Use Case- Authentication

This is the use case approach for authentication of any user.

![use-case-authentication](img/UseCaseAuth.jpg)

## Use Case- Connections

This is the use case approach for connections(Friends+ Channels) of any user.

![use-case-connection](img/UseCaseConnection.jpg)

## Use Case- Messaging

This is the use case approach for messaging of any user.

![use-case-message](img/UseCaseMessage.jpg)

# Activity Diagrams

This section will describe all the activity diagrams used in the application.

## Activity Diagram- Authentication

This is the activity diagram for authentication of any user.

![activity-diagram-authentication](img/ActivityDiagramAuth.jpg)

## Activity Diagram- Connections

This is the activity diagram for connections(Friends+ Channels) of any user.

![activity-diagram-connection](img/ActivityDiagramConnection.jpg)

## Activity Diagram- Messaging

This is the use case approach for messaging of any user.

![activity-diagram-message](img/ActivityDiagramChat.jpg)

# User Interface

The application will have following user-friendly interfaces:  
## Login Page
User will have to login first before using the application
 
## Dashboard Page
It will appear once the user is logged in. It will contain the list of 
- Subscribed Channels
- Friend’s List
- Add friend option
- Add channel option
- Chat Panel
## Search Page
User can search for different channels that he/she wants to join and user can search for other users for the workspace

# Frontend

## Reducers
This section will list all the actions and their types for the client side.
 
### actionType: `FETCH_FRIENDS_AND_CHANNELS`

`Dispatched Action` : fetchFriendsAndChannels  
`Description` : After the user logged in, this action will be dispatched and a request will be sent to the server to fetch the friend list and subscribed channel list of that user.

### actionType: `FETCH_MESSAGES_IN_CHANNEL`

`Dispatched Action` : fetchMessagesInChannel  
`Description` : When the user clicks on a subscribed channel name, this action will be dispatched and a request will be sent to the server to fetch the messages of that channel. The server will send 20 recent messages.

### actionType: `FETCH_MESSAGES_IN_DM`

`Dispatched Action` : fetchMessagesInDM  
`Description` : When the user clicks on his/her friend name, this action will be dispatched and a request will be sent to the server to fetch the messages of that personal chat. The server will send 20 recent messages.

### actionType: `CREATE_NEW_CHANNEL`

`Dispatched Action` : createNewChannel  
`Description` : When the user clicks on the **ADD CHANNEL** button and enters the required details in the add channel form, and then clicks on the final **ADD** button, this action will be dispatched and a request will be sent to the server to add that channel.

### actionType: `JOIN_CHANNEL`

`Dispatched Action` : joinChannel  
`Description` : When a user selects a channel and clicks on the **JOIN** button, this action will be dispatched and a request will be sent to the server to add that channel to the user’s subscribed channel list.

### actionType: `SEND_MESSAGE`

`Dispatched Action` : sendMessage  
`Description` : When the user types a message and clicks on the **SEND** button, this action will be dispatched and a request will be sent to the server. The server will add a message object in DB and socket.io will send that message to the concerned recipient.

### actionType: `EDIT_MESSAGE`

`Dispatched Action` : editMessage  
`Description` : When the user clicks on the message options button, the **EDIT** option will appear. When the user edits the message and confirms the edit, this action will be dispatched and a PATCH request will be sent to the socket, server and send necessary details to DB and receiver side.

### actionType: `DELETE_MESSAGE`

`Dispatched Action` : deleteMessage  
`Description` : When the user clicks on the message options, the **DELETE** option will appear. When the user deletes the message and confirms the delete, this action will be dispatched and a delete request will be sent to the server. The server will delete that message from the database and inform it to the recipient.
