**Table of Contents**

[[_TOC_]]

# User Registration Validations

## Fields

- ### Name
- ### Email
- ### Password
- ### Confirm Password
- ### User Profile Pic

## Validations

- ### Email

    - Should not be blank
    - Should not be more than 50 characters
    - Should be unique
    - Should be a valid email ID 

- ### Password

    - Should not be blank
    - Should not be more than 50 characters
    - Should have a length more than 5 characters (length>=6)
    - Should be a combination of alphabets, digits & special characters
    - A minimum of one capital letter
    - A minimum of one small letter
    - A minimum of one special character

- ### Confirm Password

    - Should not be blank
    - Should match the password field

## Error Handling/ Response to Abnormal Situations

If the flow of any of the validations does not hold true, an appropriate error message will be prompted to the user for doing the needful.

# User Login Validations

## Fields

- ### Email
- ### Password

## Validations

- ### Email

    - Should not be blank
    - Should not be more than 50 characters
    - Should be a valid email ID 

- ### Password

    - Should not be blank
    - Should not be more than 50 characters
    - Should have a length more than 5 characters (length>=6)
    - Should be a combination of alphabets, digits & special characters
    - A minimum of one capital letter
    - A minimum of one small letter
    - A minimum of one special character

## Error Handling/ Response to Abnormal Situations

If the flow of any of the validations does not hold true, an appropriate error message will be prompted to the user for doing the needful.

# Create Channel Validations

## Fields

- ### Channel Name
- ### Channel Description

## Validations

- ### Channel Name

    - Should not be blank
    - Should not be more than 50 characters
    - Should be more than 5 characters
    - Blank spaces are not allowed
    - Alphabets, digits and special characters are allowed
    - Every channel should have unique name

- ### Channel Description

    - Should not be blank
    - Should not be more than 150 characters
    - Should have more than 10 characters
    - Alphabets, digits and special characters are allowed

## Error Handling/ Response to Abnormal Situations

If the flow of any of the validations does not hold true, an appropriate error message will be prompted to the user for doing the needful.

# Send Message Validations

## Fields

- ### Message

## Validations

- ### Message

    - Should not be blank

## Error Handling/ Response to Abnormal Situations

If the flow of any of the validations does not hold true, an appropriate error message will be prompted to the user for doing the needful.














